///////////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 9/24/19
// This program prompts the user to enter two doubles for a
// height and radius and uses two methods called getSurfaceArea
// and getVolume to calculate the surface area and volume of a
// cone using the height and radius chosen by the user.
// The program prints out the results.
///////////////////////////////////////////////////////////////

import java.util.Scanner;                   // importing package to use scanner

public class E4_22 {
    public static class IceCreamCone {
        private double height;              // initializing attribute height
        private double radius;              // initializing attribute radius
        // constructor that sets height and radius
        IceCreamCone() {
            height = getHeight();
            radius = getRadius();
        }
        // method to set and return height
        private double getHeight() {
            // prompting user to enter a number for the height
            System.out.println("Please enter a number to represent the cones height: ");
            // setting height from user input
            Scanner inH = new Scanner(System.in);
            height = inH.nextDouble();
            // returning height
            return(height);
        }
        // method to set and return radius
        private double getRadius() {
            // prompting th user to enter a number for the radius
            System.out.println("Please enter a number to represent the cones radius: ");
            // setting radius from user input
            Scanner inR = new Scanner(System.in);
            radius = inR.nextDouble();
            // returning radius
            return(radius);
        }
        // method to calculate and return surface area of cone
        public void getSurfaceArea() {
            System.out.println("The surface area of the cone is: " +
                    (3.14 * radius * (Math.sqrt(Math.pow(height, 2) + Math.pow(radius, 2)))));
        }
        // metthod to calculate and return volume of cone
        public void getVolume() {
            System.out.println("The volume of the cone is: " + 3.14 * Math.pow(radius, 2) * height / 3);
        }
    }
    public static void main(String[] args) {
        // constructing new cone, setting height and radius
        IceCreamCone newCone = new IceCreamCone();
        // getting and printing surface area and volume
        newCone.getSurfaceArea();
        newCone.getVolume();
    }
}
